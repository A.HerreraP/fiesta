function bebidas(){

    let lienzo = document.getElementById('draw');
    let ctx = lienzo.getContext("2d");

    ctx.beginPath();
    ctx.fillStyle = "#FF0000"
    ctx.arc(400, 300,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(400, 250,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(400, 350 ,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(300,280,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(300,330,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.fill();


    let beb1 = new Bebida("Vodka","Smirnoff","Botella");
    let beb2 = new Bebida("Vodka2 ","Smirnoff","Botella");
    let beb3 = new Bebida("Aguardiente ","Nectar verde", "Litro y medio");
    let beb4 = new Bebida("Aguardiente2 ","Nectar verde", "Botella");
    let beb5 = new Bebida("Tequila ","Jose cuervo", "Botella");
    let beb6 = new Bebida("Jugo","Hit","litro");

    b1= " "+beb1.nombre + " " ;
    b2= " "+beb2.nombre + " " ;
    b3= " "+beb3.nombre + " " ;
    b4= " "+beb4.nombre + " " ;
    b5= " "+beb5.nombre + " " ;
    b6= " "+beb6.nombre + " " ;

    let lista1 = [b1, b2, b3, b4, b5, b6];


    document.getElementById('beb').innerHTML= lista1;
    beb1.vodka();
    beb2.vodka();
    beb3.aguardiente();
    beb4.aguardiente();
    beb5.tequila();
    beb6.jugo();

}



function invitados(){

    let lienzo = document.getElementById('draw');
    let ctx = lienzo.getContext("2d");

    ctx.beginPath();
    ctx.fillStyle = "#F700FF"
    ctx.arc(550,300,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(190,300,21,0,Math.PI*2,true);  
    ctx.closePath();
   
    ctx.fill();

    ctx.beginPath();
    ctx.fillStyle = "#0017FF"
    ctx.arc(550,350,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(550,250,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(190,350,21,0,Math.PI*2,true);  
    ctx.closePath();
    ctx.arc(190,250,21,0,Math.PI*2,true);  
    ctx.closePath();
  
    ctx.fill();


    let inv1 = new Invitados("Camila ", true, true, true);
    let inv2 = new Invitados("Camilo ", true, true, false);
    let inv3 = new Invitados("Jhonathan ",true, false, true);
    let inv4 = new Invitados("Juan ", true, true, true);
    let inv5 = new Invitados("Andres ", true, true, true);
    let inv6 = new Invitados("Alejandra ", true, true, true);
   

    e1 = " "+ inv1.nombre +" ";
    e2 = " "+ inv2.nombre +" ";
    e3 = " "+ inv3.nombre +" ";
    e4 = " "+ inv4.nombre +" ";
    e5 = " "+ inv5.nombre +" ";
    e6 = " "+ inv6.nombre +" ";
   

    let lista2 = [e1, e2, e3, e4, e5, e6];

    document.getElementById('inv').innerHTML= lista2;

    inv1.baila();
    inv2.beber();
    inv3.jugar();
    inv4.beber();
    inv1.jugar();
    inv5.baila();
    inv6.baila();
    inv6.beber();
    inv3.baila();



}
